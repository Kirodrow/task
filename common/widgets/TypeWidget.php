<?php
namespace common\widgets;

use app\models\Type;
use Yii;
use yii\base\Widget;

class  TypeWidget extends Widget {

    public $tpl;
    public $data;
    public $tree;
    public $typeHtml;


    public function init()
    {
      $this->tpl = 'select.php';

    }

    public function run()
    {
        $this->data = Type::find()->indexBy('id')->asArray()->all(); //индексация по полю id

       $this->tree = $this->getTree($this->data);
       $this->typeHtml = $this->getHtml($this->tree);
        //debug($this->tree);
        return $this->typeHtml;


    }



   /* protected function getTree($data){

        $references = array();
        foreach ($data as $id => &$node){

            $references[$node['id']] = &$node;

            $node['children'] = array();

            if($node['parent_id'] == 0){
                $tree[$node['id']] = &$node;
            }
            else $references[$node['parent_id']]['children'][$node['id']] = &$node; // Этот амперсанд дает еще одну вложенность (третий уровень)

            unset($node);
        }
        return $tree;

    }*/

    protected function getHtml($tree){
        $str = '';
        foreach ($tree as $name){
            $str .= $this->cat($name);
        }
        return $str;
    }
    protected function cat($name){
        ob_start();
        include __DIR__.'/list/list.php';
        return ob_get_clean();
    }

   protected function getTree(){
        $tree = [];
        foreach ($this->data as $id => &$value){

            if($value['parent_id'] == 0){
              $tree[] = &$value;
            }
            else $this->data[$value['parent_id']]['childs'][$value['id']] = &$value; // Этот амперсанд дает еще одну вложенность (третий уровень)
        }

        return $tree;

   }
}
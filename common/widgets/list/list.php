<li >
    <a href="" class="ajax neon" id="<?= $name['id']?>" value="<?= $name['name']?>"> <?= $name['name']?>
        <?php if( (isset($name['childs'])) ): ?>
            <span class="badge"><i class="fa fa-plus"></i></span>
        <?php endif;?>
    </a>
    <?php if( isset($name['childs']) ): ?>
        <ul>
            <?= $this->getHtml($name['childs'])?>
        </ul>
    <?php endif;?>
</li>
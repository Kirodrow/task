<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [



        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        'useFileTransport' => false,

        'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com', // хост почтовго сервера
                'username' => 'Kirodrow2@gmail.com',
                'password' => 'Kirodrowphp',
                'port' => '587', // порт сервера
                'encryption' => 'tls', // тип шифрования
            ],
        ],


        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                'main' => 'site/index',
                'about' => 'site/about',
                'ajax' => 'site/ajax',
                'json' => 'site/json',
                'delete' => 'site/delete',
                'unset' => 'site/unset',
                'type' => 'site/type',
                'contacts' => 'site/contact',
                'test'=> 'site/test',
                'group' => 'site/group',
                'lesson' => 'site/lesson'
            ],
        ],

    ],
    'params' => $params,
];
<?php namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use Yii;

class ProjectForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $name;
    public $description;
    public $short_description;
    public $status;

    public function rules()
    {
        return [
            [['name','short_description','status'], 'required']
        ];
    }


    public function attributeLabels()
    {
        return [

            'short_description' => 'Short description',
            'name' => 'Имя задачи',
            'status' => 'Тип задачи',
            'text' => 'Add Comments',
        ];
    }
}
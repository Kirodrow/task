<?php
namespace frontend\controllers;

use app\models\Comments;
use app\models\ProjectForm;
use app\models\TableForm;
use app\models\Tasks;

use Yii;

use yii\web\Controller;
use yii\filters\VerbFilter;



/**
 * Site controller
 */
class SiteController extends Controller
{
    //public $layout = 'main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            /* 'access' => [
                 'class' => AccessControl::className(),
                 'only' => ['logout', 'signup'],
                 'rules' => [
                     [
                         'actions' => ['signup'],
                         'allow' => true,
                         'roles' => ['?'],
                     ],
                     [
                         'actions' => ['logout'],
                         'allow' => true,
                         'roles' => ['@'],
                     ],
                 ],
             ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'basic';
        $todo = Tasks::find()->where(['status' => 1])->asArray()->all();
        $doing = Tasks::find()->where(['status' => 2])->asArray()->all();
        $done = Tasks::find()->where(['status' => 3])->asArray()->all();

        return $this->render('index', compact('todo', 'doing', 'done'));
    }

    public function actionAjax()

    {
        $this->layout = 'basic';
        $model = new ProjectForm();
        $comments = new Comments();
        $session = Yii::$app->session;
        $session->open();
        if (Yii::$app->request->isAjax) {
            if(isset($_POST['id'])){
                $id = $_POST['id'];
                $session->set('id', $id);
                $data = Tasks::find()->where(['id' => $id])->With('comments')->asArray()->one();
               // debug($_POST);

                return $this->render('task', compact('model', 'data', 'comments', 'id'));
            }
           else{
               return $this->render('task', compact('model','comments' ));
           }

        }
        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post("ProjectForm");
            $comment = Yii::$app->request->post("Comments");
           // $post['id'] = $id;
            debug($_SESSION);
            $save = new Tasks();
            if(isset($session['id'])){
                $save = Tasks::findOne($session['id']);
            }


            $save->name  = $post['task'];
            $save->short_description = $post['short_description'];
            $save->description = '';
            $save->status = $post['status'][0];

            $save->save();
            if($comment['text'] != null && isset($session['id'])) {
                $comments->task_id = $_SESSION['id'];
            }
            else{
                $id = Yii::$app->db->getLastInsertID();
                $comments->task_id = $id;
            }


             $comments->text = $comment['text'];
             $comments->save();

            $session->destroy();
            $this->redirect('/');
        }
    }
    public function actionJson()   {
        $this->layout = 'basic';
        $data = Tasks::find()->With('comments')->asArray()->all();
        Yii::$app->response->sendContentAsFile(\yii\helpers\Json::encode($data), 'xxx.json');

        Yii::$app->end();
        return $this->render('json');
    }

}




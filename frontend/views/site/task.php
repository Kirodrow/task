<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;


?>


    <?php $form = ActiveForm::begin() ?>
<?php
    if (isset($data)){
    $name = $data['name'];
        $short_description = $data['short_description'];
}
else {
    $name = '';
    $short_description = '';
}

?>
   <?= $form->field($model, 'task')->textInput(['value' => $name]) ?>
<?= $form->field($model, 'short_description')->textInput(['value' => $short_description]) ?>
<?php
if(isset($data)){
foreach ($data['comments'] as $m) {?>
    <h3>comments</h3>
    <div class="panel panel-default">
  <div class="panel-body"><?php
    echo $m['text'];
?>
</div>
</div>
<?php
}
}
?>

<?= $form->field($comments, 'text')->textInput(['placeholder' => 'Напишите комментарий (необязательно)']);


?>
<?= $form->field($model, 'status')->checkboxList([

    '1' => 'TODO', '2' => 'DOING', '3' => 'DONE'
])?>


<?= Html::submitButton('сохранить результат', ['class' => 'btn btn-primary',
    'type' => 'submit']);?>
<?php ActiveForm::end()?>

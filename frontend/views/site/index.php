<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Group;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>



<div class="section">
    <div class="container">
        <h2>Заполнение расписания</h2>
        <div class="row">
            <!-- Pricing Plans Wrapper -->
            <div class="pricing-wrapper col-md-12">
                <!-- Pricing Plan -->
                <div class="pricing-plan"><h3>TODO</h3>
                    <?php
                    foreach ($todo as $value){
                        echo Html::a($value['name'], Url::to('/ajax'),
                            ['id' => $value['id'], 'class' => 'ajax']);?>
                        <br>
                        <?php
                        echo "\n".$value['short_description']."\n";
                        ?>
                        <br><hr>
                        <?php

                        //echo $value['short_description'];

                    }

                    ?>
                    <!-- Pricing Plan Ribbon -->
                    <div class="ribbon-wrapper">

                    </div>

                </div>
                <!-- End Pricing Plan -->
                <div class="pricing-plan">
                    <h3>DOING</h3>
                    <?php
                    foreach ($doing as $value){
                        echo Html::a($value['name'], Url::to('/ajax'),
                            ['id' => $value['id'], 'class' => 'ajax']);?>
                        <br>
                        <?php

                    echo "\n".$value['short_description'];?>
                        <br><hr>
                        <?php


                        //echo $value['short_description'];

                    }?>
<br>


                </div>
                <!-- Promoted Pricing Plan -->
                <div class="pricing-plan">
                    <h3>DONE</h3>
                    <?php

                    foreach ($done as $value){
                        echo Html::a($value['name'], Url::to('/ajax'),
                            ['id' => $value['id'], 'class' => 'ajax']);?>
                        <br>
                        <?php
                        echo "\n".$value['short_description'];?>
                        <br><hr>
                        <?php


                        //echo $value['short_description'];

                    }?>

                </div>
            </div>

            <?php ActiveForm::end() ?>
            <script>
                function confirmDelete() {

                    if (confirm("Вы подтверждаете удаление?")) {

                    } else {

                        return false;

                    }

                }
            </script>

            <?= Html::a('Добавить Задачу', Url::to('/ajax'), ['class' => 'ajax btn btn-primary'])?>
            <?= Html::a('cкачать json', Url::to('/json'), ['class' => 'btn btn-primary'])?>



            <!--
    End Pricing Plans Wrapper -->
        </div>
    </div>
</div>









<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Mailer;
?>
<!-- banner -->
<div class="banner">
    <div class="page-head">

    </div>
</div>
<!-- //banner -->
<div class="contact_page">
    <div class="container">
        <h3 class="title">Контакты</h3>
        <div class="col-md-8 contact-grids1 animated wow fadeInRight" data-wow-delay=".5s">

            <?php if( Yii::$app->session->hasFlash('success') ): ?>


                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close flash" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <?php echo Yii::$app->session->getFlash('success'); ?>

                </div>
                <?php else :;?>



            <?php
            $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
            ]) ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'mail')?>

            <?= $form->field($model, 'type')->textInput(['id'=>'information',
                'class'=> 'zbz-input-clearable form-control', 'value' => '...'])   ?>




            <ul class="catalog">
                <?= \common\widgets\TypeWidget::widget()?>
            </ul>
           <?= $form->field($model, 'problem')->textarea(['rows' => '6'])?>

            <div class="form-group">
                <div class="col-lg-offset-1 col-lg-11">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary',
                        'type' => 'submit']) ?>
                </div>
            </div>
            <?php ActiveForm::end() ?>


<?php endif;?>
        </div>

        <div class="col-md-4 contact-grids">
            <div class=" contact-grid animated wow fadeInLeft" data-wow-delay=".5s">
                <div class="contact-grid1">
                    <div class="contact-grid2 ">
                        <i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
                    </div>
                    <div class="contact-grid3">
                        <h4>Address</h4>
                        <p>Ул кунцевщина 25-156 <span>Минск</span></p>
                    </div>
                </div>
            </div>
            <div class=" contact-grid animated wow fadeInUp" data-wow-delay=".5s">
                <div class="contact-grid1">
                    <div class="contact-grid2 contact-grid4">
                        <i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
                    </div>
                    <div class="contact-grid3">
                        <h4>Телефон</h4>
                        <p>+375447727100<span>второй телефон</span></p>
                    </div>
                </div>
            </div>
            <div class=" contact-grid animated wow fadeInRight" data-wow-delay=".5s">
                <div class="contact-grid1">
                    <div class="contact-grid2 contact-grid5">
                        <i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
                    </div>
                    <div class="contact-grid3">
                        <h4>Email</h4>
                        <p><a href="mailto:info@example.com">info@example1.com</a><span><a href="mailto:info@example.com">info@example2.com</a></span></p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="map">
    <div class="container">
        <h3 class="title">посмотреть на карте</h3>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2350.023102216502!2d27.43072631585991!3d53.913565380101915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbc4d315efc69f%3A0x8faba859f3ced54e!2z0YPQu9C40YbQsCDQmtGD0L3RhtC10LLRidC40L3QsCAyNSwg0JzQuNC90YHQug!5e0!3m2!1sru!2sby!4v1509525575985" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
